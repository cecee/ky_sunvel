package com.cecee.ky_sunvel;


import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.LTGRAY;
import static android.graphics.Color.WHITE;
import static android.provider.Settings.System.getString;


public class RecyclerAdapterScale extends RecyclerView.Adapter<RecyclerAdapterScale.ScaleViewHolder> {
    private static final String TAG = "#@#";
    private Context context;
    private Current_Status currentStatus;
    private int[] scale;
    private  int[] param;
    private int[] bg_color;
    private String str;

    public static class ScaleViewHolder extends RecyclerView.ViewHolder {
        //recycleView안에 단위블럭을 v로 받고 v에 구성되어 있는 각각의 view를 인스턴스화한다.
        //지금은 텍스트뷰 하나밖에 없다.
        private final LinearLayout recycle_scale;
        private final TextView textViewScaleValue;
        private final Button buttonScalePos;

        public ScaleViewHolder(View v) {
            super(v);
            recycle_scale = v.findViewById(R.id.recycle_scale);
            textViewScaleValue = v.findViewById(R.id.textViewScaleValue);
            buttonScalePos = v.findViewById(R.id.buttonScalePos);
            // relativelayout_recycle_scale=v.findViewById(R.id.relativelayout_recycle_scale);
        }
    }

    public RecyclerAdapterScale(Current_Status dataSet) {
        //currentStatus = dataSet;
       // this.bg_color = dataSet.getItemViewColor();
        this.scale = dataSet.getScale_data();
        //Log.d("#@#","@@@@@@@@@@@ScaleAdapter scale:->"+scale[2]);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScaleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        //똥가리 전체를 감싼뷰
        //scale = scaleData.getArryValue();
        //Log.d("#@#","scale:->"+scale.toString());
        LinearLayout v = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_scale_item, viewGroup, false);//xml file 이름
        ScaleViewHolder vh = new ScaleViewHolder(v);
        context = viewGroup.getContext();
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ScaleViewHolder viewHolder, final int position) {
        //int index[] = {0, 7, 1, 8, 2, 9, 3, 10, 4, 11, 5, 12, 6, 13};
        int index[] = {0, 1, 2, 3, 4, 5};
        int pos = index[position];
        //int[] bg_color = ((MainActivity) context).currentStatus.getItemViewColor();
         bg_color = ((MainActivity) context).currentStatus.getItemViewColor();

          param= ((MainActivity) context).currentStatus.getCurrent_parameter();
        //Log.d(TAG,"#@# @@@@@@@@@@(x) onBindViewHolder ->"+ Arrays.toString(param));
        int item_Weight_min=param[2];
        int item_Weight_max=param[3];

        LinearLayout.LayoutParams params_pos = (LinearLayout.LayoutParams) viewHolder.buttonScalePos.getLayoutParams();
        int _color;
        if(((MainActivity) context).TEST ) bg_color[pos]=0;
        switch (bg_color[pos]) {
            case 1:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_1);
                break;
            case 2:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_2);
                break;
            case 3:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_3);
                break;
            case 4:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_4);
                break;
            default:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.white);
                break;
        }

        if(scale[pos]<item_Weight_min && scale[pos]>5 && !((MainActivity) context).TEST ) {
            //_color = ContextCompat.getColor(((MainActivity) context), R.color.combi_weight_min);
            str = ((MainActivity) context).getString(R.string.emogi_irregular);
            str =String.format("%s %d",str,scale[pos]);
           // str ="미달";
        }
        else if(scale[pos]>item_Weight_max && !((MainActivity) context).TEST) {
            //_color = ContextCompat.getColor(((MainActivity) context), R.color.combi_weight_max);
            str = ((MainActivity) context).getString(R.string.emogi_irregular);
            str =String.format("%s %d",str,scale[pos]);
            //str ="초과";
        }
        else{
            str = String.valueOf(scale[pos]);
        }

        //str = String.valueOf(scale[pos]);
        viewHolder.textViewScaleValue.setText(str);
        str = String.valueOf(pos + 1);
        viewHolder.buttonScalePos.setText(str);
        viewHolder.textViewScaleValue.setBackground(ContextCompat.getDrawable(context, R.drawable.recycler_item_bg));
        viewHolder.textViewScaleValue.getBackground().setColorFilter(_color, PorterDuff.Mode.SRC_ATOP);

        GradientDrawable circle_gd = new GradientDrawable();
        circle_gd.setColor(_color);
        circle_gd.setCornerRadius(45);

        if (bg_color[pos] == 0) {
            viewHolder.textViewScaleValue.setTextColor(LTGRAY);
            viewHolder.buttonScalePos.setTextColor(LTGRAY);
            circle_gd.setStroke(4, LTGRAY);
        } else {
            viewHolder.textViewScaleValue.setTextColor(WHITE);
            viewHolder.buttonScalePos.setTextColor(WHITE);
            circle_gd.setStroke(4, WHITE);
        }

//        viewHolder.buttonScalePos.setBackgroundDrawable(circle_gd);
//        if (position % 2 == 0) {
//            params_pos.setMargins(390, 0, 0, 0);
//            viewHolder.buttonScalePos.setLayoutParams(params_pos);
//        } else {
//            params_pos.setMargins(10, 0, 0, 0);
//            viewHolder.buttonScalePos.setLayoutParams(params_pos);
//        }
    }

    @Override
    public int getItemCount() {
       // return 14;
        return 6;
        // return (scale==null) ? 0 : scale.length;
    }


}