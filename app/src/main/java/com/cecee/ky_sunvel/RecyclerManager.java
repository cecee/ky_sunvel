package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import needle.Needle;
import needle.UiRelatedTask;

public class RecyclerManager {

    Context mContext;
    public RecyclerView.Adapter mAdapter_scale, mAdapter_history;
    public RecyclerView.LayoutManager mLayoutManager_scale, mLayoutManager_history;

    public RecyclerManager(Context context) {
        mContext = context;
    }

    public void recycle_scale_init(){
        ((MainActivity) mContext).recycleView_scale.setHasFixedSize(true);
        mLayoutManager_scale = new GridLayoutManager(mContext, 3);
        ((MainActivity) mContext).recycleView_scale.setLayoutManager(mLayoutManager_scale);
        mAdapter_scale = new RecyclerAdapterScale(((MainActivity) mContext).currentStatus);
        ((MainActivity) mContext).recycleView_scale.setAdapter(mAdapter_scale);
    }
    public void recycler_history_init(){
        ((MainActivity) mContext).recycleView_history.setHasFixedSize(true);
        mLayoutManager_history = new GridLayoutManager(mContext, 1);
        ((MainActivity) mContext).recycleView_history.setLayoutManager(mLayoutManager_history);
        mAdapter_history = new RecyclerAdapterHistory(((MainActivity) mContext).currentStatus);
        ((MainActivity) mContext).recycleView_history.setAdapter(mAdapter_history);
    }

    public void recycler_history_update() {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONArray>() {
            String str_from = (String) ((MainActivity) mContext).textView_date_from.getText();
            String str_to = (String) ((MainActivity) mContext).textView_date_to.getText();

            @Override
            protected JSONArray doWork() {
                JSONArray mArry = ((MainActivity) mContext).db_manager.dbGetCurrentProductForRecycler(str_from, str_to, false);
                return mArry;
            }
            @Override
            protected void thenDoUiRelatedWork(JSONArray result) {
                ((MainActivity) mContext).currentStatus.setProduct_jArry(result);
                recycler_history_init();
                Log.d("#@#", "#@# [" + result.length() + "]개  리사이클뷰그리고 다음페이지 aaaa->" + result.toString());
                int total_weight=0;
                int total_box_num=0;
                int total_item_num=0;
                float favr_box_weight, favr_box_item;
                for(int i=0 ;i<result.length();i++){
                    try {
                        JSONObject obj=result.getJSONObject(i);
                        total_weight= total_weight + obj.getInt("itotal_amount");
                        total_box_num=total_box_num+obj.getInt("itotal_box_cnt");
                        total_item_num=total_item_num+obj.getInt("itotal_item_cnt");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                favr_box_weight=(float)total_weight/total_box_num;
                favr_box_item=(float)total_item_num/total_box_num;
                String str_cm = String.format("%.2f Kg", total_weight/1000.00f);
                DecimalFormat myFormatter = new DecimalFormat("###,###");
                String formattedStringNum = myFormatter.format(total_box_num);
                String str_cg = String.format("%s 개", formattedStringNum);
                String str_bm = String.format("%.2f g", favr_box_weight);
                ((MainActivity) mContext).textView_chong_mugae.setText(str_cm);
                ((MainActivity) mContext).textViewChongBox.setText(str_cg);
                ((MainActivity) mContext).textViewBoxAvrMugae.setText(str_bm);

                mAdapter_history.notifyDataSetChanged();

            }
        });
    }
}
