package com.cecee.ky_sunvel;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecyclerAdapterHistory extends RecyclerView.Adapter<RecyclerAdapterHistory.ScaleViewHolder> {

    private Context context;
    private int[] scale;
    private String str;
    JSONArray history_jarry;

    public static class ScaleViewHolder extends RecyclerView.ViewHolder {
        //recycleView안에 단위블럭을 v로 받고 v에 구성되어 있는 각각의 view를 인스턴스화한다.
        //지금은 텍스트뷰 하나밖에 없다.
        //private final Button mButton,mBtnCircle;
       // private final LinearLayout mLinear0,mLinear1;
        //private final TextView textViewScalePos,textViewScaleValue;
        private final TextView textView_history_date,textView_history_total_weight,
                textView_history_total_box_cnt,textView_history_average_box_weight,textView_history_average_box_cnt;

        public ScaleViewHolder(View v) {
            super(v);
            textView_history_date=v.findViewById(R.id.textView_history_date);
            textView_history_total_weight=v.findViewById(R.id.textView_history_total_weight);
            textView_history_total_box_cnt=v.findViewById(R.id.textView_history_total_box_cnt);
            textView_history_average_box_weight=v.findViewById(R.id.textView_history_average_box_weight);
            textView_history_average_box_cnt=v.findViewById(R.id.textView_history_average_box_cnt);
        }
    }

    public RecyclerAdapterHistory(Current_Status dataSet) {
        history_jarry=dataSet.getProduct_jArry();
        Log.d("#@#", "@@@@@@@@@@@@@@@@RecyclerAdapterHistory~~~"+history_jarry.length());
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScaleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_history_item, viewGroup, false);//xml file 이름
        ScaleViewHolder vh= new ScaleViewHolder(v);
        context = viewGroup.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(ScaleViewHolder viewHolder, final int position) {
       // Log.d("#@#","@@@@@@@@@@@@@@@@onBindViewHolder->"+history_jarry.length());
        int item_cnt = history_jarry.length();
        JSONObject jobj;
        String str_date, str_total_amount, str_total_box_cnt, str_average_box_weight, str_average_box_item_cnt;
             try {
                jobj = history_jarry.getJSONObject(position);
                 str_date = jobj.getString("stoday");
                 str_total_amount=  String.format("%.1f Kg",jobj.getInt("itotal_amount")/1000.0f);//String.valueOf(jobj.getInt("total_amount"));
                 str_total_box_cnt= String.valueOf(jobj.getInt("itotal_box_cnt"));
                 str_average_box_weight=String.format("%.1f Kg",jobj.getDouble("faverage_box_weight")/1000.0f);// String.valueOf(jobj.getInt("average_box_weight"));
                 str_average_box_item_cnt=String.format("%.1f 개",jobj.getDouble("faverage_box_item_cnt"));// String.valueOf(jobj.getInt("average_box_item_cnt"));
                 viewHolder.textView_history_date.setText(str_date);
                 viewHolder.textView_history_total_weight.setText(str_total_amount);
                 viewHolder.textView_history_total_box_cnt.setText(str_total_box_cnt);
                 viewHolder.textView_history_average_box_weight.setText(str_average_box_weight);
                 viewHolder.textView_history_average_box_cnt.setText(str_average_box_item_cnt);
                 //Log.d("#@#","@@@@@@@@@@@@@@@@@@@ts:->"+ts);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (history_jarry==null) ? 0 : history_jarry.length();
    }
}