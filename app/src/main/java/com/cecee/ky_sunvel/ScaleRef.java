package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ScaleRef implements Serializable {
    private static final String TAG = "#@#";
    private String current_sname="";
    Context mContext;

   // private JSONArray objParamArry;

    public String getSname() {
        return current_sname;
    }
    public void setSname(String sname) {
        this.current_sname = sname;
    }



    ScaleRef(Context context){
        mContext = context;
        this.current_sname= "";
       // this.objParamArry=new JSONArray();
    }
    public void setRef(int id, int tare, int i1k, int i3k, int i5k) {

    }

    public int[] REFDB_selectById(int id) {
        int[] mRef=new int[4];
        String query= "SELECT * FROM public.scale_ref where id=%d;";
        query=String.format(query, id);
        Connection conn  = PgConn.openDB();
        try{
            Statement st = conn.createStatement();
            ResultSet rs;
            rs = st.executeQuery(query);
            Log.d(TAG,"REFDB_select"+ rs.toString());
            while (rs.next()) {
                mRef[0]=rs.getInt("itare");
                mRef[1]=rs.getInt("i1K");
                mRef[2]=rs.getInt("i3K");
                mRef[3]=rs.getInt("i5K");
            }
            conn.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return mRef;
    }
    public void REFDB_upsertById(int id, int[] mRef){
        int  itare=mRef[0];
        int  i1k=mRef[1];
        int  i3k=mRef[2];
        int  i5k=mRef[3];
        String query=
                "INSERT INTO public.scale_ref (id, itare, i1k, i3k, i5k)"+
                        "VALUES(%d, %d, %d, %d, %d)"+
                        "ON CONFLICT (id)"+
                        "DO update SET itare=%d, i1k=%d, i3k=%d, i5k=%d ;";
        query=String.format(query, id, itare, i1k, i3k, i5k, itare, i1k, i3k, i5k);
        Connection conn  = PgConn.openDB();
        try{
            Statement st = conn.createStatement();
            ResultSet rs;
            rs = st.executeQuery(query);
            //Log.d(TAG, rs.toString());
            conn.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public  void REFDB_upsertTare(int id, int itare){

        String query=
                "INSERT INTO public.scale_ref (id, itare)"+
                        "VALUES(%d, %d)"+
                        "ON CONFLICT (id)"+
                        "DO update SET itare=%d;";
        query=String.format(query, id, itare, itare);
        Log.d(TAG,"REFDB_upsertTare query: "+ query);
        Connection conn  = PgConn.openDB();
        try{
            Statement st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }



    public void REFDB_upsertToAllItemsBysname(String str_item, int[] data){
        int  icombi_min=data[0];
        int  icombi_max=data[1];
        int  iweight_min=data[2];
        int  iweight_max=data[3];
        int  ibox_min=data[4];
        int  ibox_max=data[5];
        String query=
                "INSERT INTO public.all_items (sname, icombi_min, icombi_max, iweight_min, iweight_max, ibox_min, ibox_max, itime)"+
                        "VALUES('%s', %d, %d, %d, %d, %d, %d, now())"+
                        "ON CONFLICT (sname)"+
                        "DO update SET sname='%s', icombi_min=%d, icombi_max=%d, iweight_min=%d, iweight_max=%d , ibox_min=%d, ibox_max=%d, itime=now() ;";
        query=String.format(query, str_item, icombi_min, icombi_max, iweight_min, iweight_max, ibox_min, ibox_max,
                str_item, icombi_min, icombi_max, iweight_min, iweight_max, ibox_min, ibox_max );
        Log.d(TAG, "#@# ##### query--->"+query);

        Connection conn  = PgConn.openDB();
        try{
            Statement st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void REFDB_deleteToAllItemsBysname(String str_item){
        String query="DELETE FROM public.all_items WHERE sname='%s'";
        query=String.format(query, str_item, str_item );
        //Log.d(TAG, "#@# ##### query--->"+query);
        Connection conn  = PgConn.openDB();
        try{
            Statement st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public  JSONArray REFDB_selectAllItems(){
        String query="SELECT * FROM public.all_items ORDER BY itime DESC";
        Connection conn  = PgConn.openDB();
        JSONArray oParamArry=new JSONArray();
        try{
            Statement st = conn.createStatement();
            ResultSet rs;
            rs = st.executeQuery(query);
            int idx=0;
            int[] mParam=new int[16];
            while (rs.next()) {
                try {
                    JSONObject jObj=new JSONObject();
                    jObj.put("sname",rs.getString("sname"));
                    jObj.put("icombi_min",rs.getInt("icombi_min"));
                    jObj.put("icombi_max",rs.getInt("icombi_max"));
                    jObj.put("iweight_min",rs.getInt("iweight_min"));
                    jObj.put("iweight_max",rs.getInt("iweight_max"));
                    jObj.put("ibox_min",rs.getInt("ibox_min"));
                    jObj.put("ibox_max",rs.getInt("ibox_max"));
                    oParamArry.put(jObj);
                    if(idx==0){
                        this.current_sname=rs.getString("sname");
                        mParam[0]=rs.getInt("icombi_min");
                        mParam[1]=rs.getInt("icombi_max");
                        mParam[2]=rs.getInt("iweight_min");
                        mParam[3]=rs.getInt("iweight_max");
                        mParam[4]=rs.getInt("ibox_min");
                        mParam[5]=rs.getInt("ibox_max");
                        ((MainActivity) mContext).currentStatus.setCurrent_parameter(mParam);
                    }
                }
                catch (JSONException e){}
                idx++;
            }
            //Log.d(TAG, "sname--->"+current_sname);
            conn.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        return oParamArry;
    }

    public  JSONObject REFDB_selectFromAllItemsBysname(String str){
        String query="SELECT * FROM public.all_items WHERE sname='%s'";
        query=String.format(query, str );
        Log.d(TAG, "#@# REFDB_selectFromAllItemsBysname query--->"+query);
         Connection conn  = PgConn.openDB();
        JSONObject jObj=new JSONObject();
        try{
            Statement st = conn.createStatement();
            ResultSet rs;
            rs = st.executeQuery(query);
            while (rs.next()) {
                try {
                    jObj.put("sname",rs.getString("sname"));
                    jObj.put("icombi_min",rs.getInt("icombi_min"));
                    jObj.put("icombi_max",rs.getInt("icombi_max"));
                    jObj.put("iweight_min",rs.getInt("iweight_min"));
                    jObj.put("iweight_max",rs.getInt("iweight_max"));
                    jObj.put("ibox_min",rs.getInt("ibox_min"));
                    jObj.put("ibox_max",rs.getInt("ibox_max"));
                }
                catch (JSONException e){
                }
            }
            conn.close();
            return jObj;
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

    }
}
