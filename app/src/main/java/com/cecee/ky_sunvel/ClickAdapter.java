package com.cecee.ky_sunvel;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import static android.graphics.Color.GRAY;
import static android.graphics.Color.RED;
import static android.graphics.Color.WHITE;
import static android.graphics.Color.YELLOW;

public class ClickAdapter implements View.OnClickListener, View.OnTouchListener, View.OnLongClickListener {
    private static final long HOLD_PRESS_TIME = 5000; //ms unit
    public int date_picker_pos = 0;
    Context mContext;
    Button btv;
    int _year, _monthOfYear, _dayOfMonth;
    DatePickerDialog date_dialog;
    TimePickerDialog time_dialog;

    public ClickAdapter(Context context) {
        mContext = context;
        date_dialog = new DatePickerDialog(mContext, date_listener, 2021, 5, 01);
        time_dialog = new TimePickerDialog(mContext, time_listener, 15, 24, false);
    }

    @Override
    public void onClick(View v) {
        //int ltitle_click_cnt=((MainActivity) mContext).title_click_cnt;
        ((MainActivity) mContext).TEST = false;
        if(v.getId()!=R.id.imageView_character)((MainActivity) mContext).title_click_cnt=0;
        switch (v.getId()) {
             case R.id.imageView_character:
                ((MainActivity) mContext).title_click_cnt++;
                 System.out.printf("#@# #####Press imageView_character  title_click_cnt[%d]\n", ((MainActivity) mContext).title_click_cnt);
                 if(((MainActivity) mContext).title_click_cnt>10){
                     ((MainActivity) mContext).title_click_cnt=0;
                     ((MainActivity) mContext).TEST = true;
                     ((MainActivity) mContext).textView_b1_title.setText("FACTORY_SETTING");
                 }
                //this.change_menu(3);
                break;
            case R.id.button_zero:
                System.out.printf("#@# #####Press button_zero  menu_position[%d]\n", ((MainActivity) mContext).menu_position);
                //this.change_menu(3);
                break;
            case R.id.button_naeyek:
                //System.out.printf("#@# #####Press button_naeyek  menu_position[%d]\n", ((MainActivity) mContext).menu_position);
                this.change_menu(3);
                break;

            case R.id.button_naeyek_return:
            case R.id.seljung_return:
                //System.out.printf("#@# #####Press button_naeyek_return  menu_position[%d]\n", ((MainActivity) mContext).menu_position);
                this.change_menu(0);
                break;

            case R.id.button_seljung:
               // System.out.printf("#@# #####Press button_seljung  menu_position[%d]\n", ((MainActivity) mContext).menu_position);
                this.change_menu(2);
                break;

            case R.id.textView_z00_date:
                date_picker_pos = 10;
                time_dialog.show();
                date_dialog.show();
                break;
//            case R.id.button_z01_0_adjust://setting tare
//                ((MainActivity) mContext).db_manager.dbUpdateZeroAdjust();
//                break;
            case R.id.button_add_item:
                this.add_spinner_item_show();
                break;
            case R.id.button_remove_item:
                this.remove_spinner_item_show();
                break;
//            case R.id.button_z01_data:
//                this.change_menu(2);
//                break;
            case R.id.imageView_date_from:
                date_picker_pos = 0;
                date_dialog.show();
                break;
            case R.id.imageView_date_to:
                date_picker_pos = 1;
                date_dialog.show();
                break;
            case R.id.button_history:
                ((MainActivity) mContext).recyclerManager.recycler_history_update();
                break;
            case R.id.ButtonView_system_clear:
                ((MainActivity) mContext).currentStatus.system_total_box_num = 0;
                ((MainActivity) mContext).currentStatus.system_total_weight = 0;
                ((MainActivity) mContext).textView_system_box_num.setText("0 개");
                ((MainActivity) mContext).textView_system_total_weight.setText("0.00 Kg");
                ((MainActivity) mContext).textView_system_box_average_weight.setText("0 g");
                break;
            case R.id.ButtonView_combin_clear:
                ((MainActivity) mContext).RE_COMBINATION=true;
                break;


            case R.id.textView_test_mode:
                if (((MainActivity) mContext).TEST) {
                        ((MainActivity) mContext).db_manager.dbUpsert_1KFactor();
                }
                break;
            case R.id.textView_test_zero:
                if (((MainActivity) mContext).TEST) {
                    ((MainActivity) mContext).db_manager.dbUpdateZeroAdjust();
                }
                break;

        }
    }
    @Override
    public boolean onLongClick(View v) {
//        System.out.printf("#@# #####ON-LONGCLICK button_zero  menu_position[%d]\n", v.getId());
//        switch (v.getId()) {
//            case R.id.imageView_character:
//                ((MainActivity) mContext).TEST = true;
//                ((MainActivity) mContext).textView_b1_title.setText("FACTORY_SETTING");
//                break;
//        }
        return false;
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        long longPressTimeout = 2000;
        System.out.printf("#@# #####ON TOUCH button_zero  menu_position[%d]\n", v.getId());

        if (v.isPressed() && event.getAction() == MotionEvent.ACTION_UP) {
            long eventDuration = event.getEventTime() - event.getDownTime();
            if (eventDuration > longPressTimeout) {
                //System.out.printf("#@# #####onLongClick  menu_position[%d]\n", ((MainActivity) mContext).menu_position);
                onLongClick(v);
            } else {
               // System.out.printf("#@# #####onClick  menu_position[%d]\n", ((MainActivity) mContext).menu_position);
                onClick(v);
            }
        }
       // return false;



        //      btv = (Button) v.findViewById(R.id.button_z01_factor);
//        iv_test = (ImageView) v.findViewById(R.id.imageView_test);
        switch (v.getId()) {
//            case R.id.button_z01_factor:
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    timer.start();
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    timer.cancel();
//                    btv.setText("1K");
//                }
//                break;


            case R.id.imageView_test:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ((MainActivity) mContext).TEST = !((MainActivity) mContext).TEST;
                    if (((MainActivity) mContext).TEST) {
                        ((MainActivity) mContext).textView_test_mode.setTextColor(WHITE);
                        ((MainActivity) mContext).textView_test_zero.setTextColor(WHITE);
                        ((MainActivity) mContext).textView_test_mode.setBackgroundColor(GRAY);
                        ((MainActivity) mContext).textView_test_zero.setBackgroundColor(GRAY);
                    } else {
                        ((MainActivity) mContext).textView_test_mode.setTextColor(WHITE);
                        ((MainActivity) mContext).textView_test_zero.setTextColor(WHITE);
                        ((MainActivity) mContext).textView_test_mode.setBackgroundColor(WHITE);
                        ((MainActivity) mContext).textView_test_zero.setBackgroundColor(WHITE);
                    }
                    ((MainActivity) mContext).textView_test_mode.setText("1K Adj");
                    //TEST_PRESS_timer.start();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    //TEST_PRESS_timer.cancel();
                }
                break;
        }
        return false;
    }

    private void change_menu(int page) {
        ((MainActivity) mContext).menu_position = page;
        ((MainActivity) mContext).textView_b1_title.setText(MainActivity.MODEL_NAME);

        switch (page) {
            case 0:
                ((MainActivity) mContext).linearLayout_b1.setVisibility(View.VISIBLE);
                ((MainActivity) mContext).linearLayout0.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout1.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout2.setVisibility(View.INVISIBLE);
                break;
            case 1:
                update_spinner_item_show();//update
                ((MainActivity) mContext).linearLayout_b1.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout0.setVisibility(View.VISIBLE);
                ((MainActivity) mContext).linearLayout1.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout2.setVisibility(View.INVISIBLE);
                break;
            case 2:
                ((MainActivity) mContext).linearLayout_b1.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout0.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout1.setVisibility(View.VISIBLE);
                ((MainActivity) mContext).linearLayout2.setVisibility(View.INVISIBLE);
                break;
            case 3:
                ((MainActivity) mContext).textView_date_to.setText(((MainActivity) mContext).currentStatus.getCurrent_date());
                ((MainActivity) mContext).recyclerManager.recycler_history_update();
                ((MainActivity) mContext).linearLayout_b1.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout0.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout1.setVisibility(View.INVISIBLE);
                ((MainActivity) mContext).linearLayout2.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void add_spinner_item_show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(((MainActivity) mContext));
        LayoutInflater inflater = ((MainActivity) mContext).getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_item_add, null);
        builder.setView(view);
        final Button submit = (Button) view.findViewById(R.id.buttonSubmit);
        final EditText edittext_item = (EditText) view.findViewById(R.id.edittext_item_name);
        final AlertDialog dialog = builder.create();
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String str_item = edittext_item.getText().toString();
                int s_len = str_item.length();//길이 구하기
                if (s_len > 0) {
                    int[] data = {3, 4, 200, 300, 1500, 2000};//default value
                    ((MainActivity) mContext).db_manager.spinnerDBAddAndView(str_item, data);
                    dialog.dismiss();
                } else {
                    Toast toast = Toast.makeText(((MainActivity) mContext).getApplicationContext(), "품명을 한글자 이상 입력하세요", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP | Gravity.LEFT, 600, 100);
                    toast.show();
                }
                // Toast.makeText(getApplicationContext(), strEmail+"/"+strPassword,Toast.LENGTH_LONG).show();
                //dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void remove_spinner_item_show() {
        String selected_item_name = (String) ((MainActivity) mContext).textView_selected_title.getText();
        System.out.printf("#@# ##### current_sname[%s]\n", selected_item_name);
        ((MainActivity) mContext).db_manager.spinnerRemoveDBAndView(selected_item_name);
    }

    public void update_spinner_item_show() {
        String str, num0, num1;
        String[] num;
        String selected_item_name = (String) ((MainActivity) mContext).textView_selected_title.getText();
        int[] data = new int[6];
        str = (String) ((MainActivity) mContext).textView_comb_num.getText();
        ((MainActivity) mContext).textView_z00_combi_cnt.setText(str);
        num = str.split("-");
        num0 = num[0].replaceAll("[^0-9]", "");
        num1 = num[1].replaceAll("[^0-9]", "");
        data[0] = Integer.parseInt(num0);
        data[1] = Integer.parseInt(num1);

        str = (String) ((MainActivity) mContext).textView_combi_weight.getText();
        ((MainActivity) mContext).textView_z00_combi_weight.setText(str);
        num = str.split("-");
        num0 = num[0].replaceAll("[^0-9]", "");
        num1 = num[1].replaceAll("[^0-9]", "");
        data[2] = Integer.parseInt(num0);
        data[3] = Integer.parseInt(num1);

        str = (String) ((MainActivity) mContext).textView_box_weight.getText();
        ((MainActivity) mContext).textView_z00_weight_box.setText(str);
        num = str.split("-");
        num0 = num[0].replaceAll("[^0-9]", "");
        num1 = num[1].replaceAll("[^0-9]", "");
        data[4] = Integer.parseInt(num0) * 100;
        data[5] = Integer.parseInt(num1) * 100;
        Log.d("#@#", "@@@@@@@@@@() update_spinner_item_show ->" + selected_item_name);
        ((MainActivity) mContext).db_manager.funTitleUpdateAndView(selected_item_name, data);
    }

    public DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String str = String.format("%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
            switch (date_picker_pos) {
                case 0:
                    ((MainActivity) mContext).textView_date_from.setText(str);
                    break;
                case 1:
                    ((MainActivity) mContext).textView_date_to.setText(str);
                    break;
                case 10:
                    _year = year;
                    _monthOfYear = monthOfYear;
                    _dayOfMonth = dayOfMonth;
                    break;
            }
            //Toast.makeText(getApplicationContext(), year + "년" + monthOfYear + "월" + dayOfMonth + "일", Toast.LENGTH_SHORT).show();
        }
    };

    private TimePickerDialog.OnTimeSetListener time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar c = Calendar.getInstance();
            c.set(_year, _monthOfYear, _dayOfMonth, hourOfDay, minute, 00);
            AlarmManager am = (AlarmManager) ((MainActivity) mContext).getSystemService(Context.ALARM_SERVICE);
            am.setTime(c.getTimeInMillis());
            ((MainActivity) mContext).currentStatus.fun_CurrentDateTimeFromSystemTime();
            ((MainActivity) mContext).textView_z00_date.setText(((MainActivity) mContext).currentStatus.current_date_time);
            ((MainActivity) mContext).textView_z02_date.setText(((MainActivity) mContext).currentStatus.current_date_time);
            Toast.makeText(mContext.getApplicationContext(), hourOfDay + "시" + minute + "분", Toast.LENGTH_SHORT).show();
        }
    };
    private CountDownTimer timer = new CountDownTimer(HOLD_PRESS_TIME, 200) {
        @Override
        public void onTick(long l) {
            //Log.w("Button", "Count down..." + l); //It call onFinish() when l = 0
        }

        @Override
        public void onFinish() {
            //TODO your action here!
            // VV.setBackgroundColor(Color.RED);
            //setting 1k factor
            ((MainActivity) mContext).db_manager.dbUpsert_1KFactor();
            btv.setText("OK");
            //Log.w("Button", "Count finish...");

        }
    };

    private CountDownTimer TEST_PRESS_timer = new CountDownTimer(HOLD_PRESS_TIME, 10) {
        @Override
        public void onTick(long l) {
            //Log.w("Button", "Count down..." + l); //It call onFinish() when l = 0

        }

        @Override
        public void onFinish() {
            //TEST
            ((MainActivity) mContext).TEST = !((MainActivity) mContext).TEST;
            if (((MainActivity) mContext).TEST) {
                ((MainActivity) mContext).textView_test_mode.setTextColor(RED);
            } else ((MainActivity) mContext).textView_test_mode.setTextColor(WHITE);
        }
    };


}