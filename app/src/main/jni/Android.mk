LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libJnilib
LOCAL_SRC_FILES := ../../class/jniMain.cpp
LCOAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES += ../../Class

LOCAL_ARM_NEON := true
LOCAL_ARM_MODE := arm

LOCAL_CPPFLAGS += -std=c++1z -fno-stack-protector -g
#LOCAL_CPPFLAGS += -std=c++17 -fno-stack-protector -g
LOCAL_CPP_FEATURES := rtti exceptions

LOCAL_LDLIBS := -ldl -lGLESv1_CM -lGLESv2 -llog -landroid
include $(BUILD_SHARED_LIBRARY)

